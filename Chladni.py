import matplotlib.pyplot as plt
import matplotlib
import numpy as np
import colorsys
from moviepy.editor import VideoClip
import numpy as np
from moviepy.video.io.bindings import mplfig_to_npimage
from numba import jit

matplotlib.use("Agg")


@jit
def chladni(m, n, color1, color2):
    fig = plt.figure(figsize=(10, 10), facecolor=color2)
    ax = plt.Axes(fig, [0, 0, 1, 1])
    fig.add_axes(ax)
    ax.set_xticks([])
    ax.set_yticks([])
    ax.set_xlim(-2, 2)
    ax.set_ylim(-2, 2)

    ax.set_aspect("equal")
    ax.axis("off")

    # equation: 0 = sin(pi * n * x) * sin(pi * m * y) + sin(pi * m * x) * sin(pi * n * y)
    xCoords = []
    yCoords = []

    numDivs = int(1e3)
    for x in np.linspace(-2, 2, numDivs):
        y = np.linspace(-2, 2, numDivs)
        matches = y[
            np.abs(
                np.sin(np.pi * n * x) * np.sin(np.pi * m * y)
                + np.sin(np.pi * m * x) * np.sin(np.pi * n * y)
            )
            < 2e-1
        ]
        xCoords.extend([x] * len(matches))
        yCoords.extend(matches)

    ax.scatter(xCoords, yCoords, color=color1, s=0.1)

    return fig


@jit
def chladniHelper(t):
    m = 7 + 3 * np.sin(np.pi / 4 * t)
    n = 3

    hue1 = np.cos(0.5 * t) / 2 + 0.5
    color1 = colorsys.hsv_to_rgb(hue1, 1, 1)
    color2 = (0, 0, 0)

    return mplfig_to_npimage(chladni(m, n, color1, color2))


def main():
    animation = VideoClip(
        chladniHelper, duration=32
    )  # make duration a multiple of 8 (for looping purposes)

    animation.write_videofile("./Videos/Chladni.mp4", fps=30, threads=2)


if __name__ == "__main__":
    main()
