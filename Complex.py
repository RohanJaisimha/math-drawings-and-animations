import matplotlib.pyplot as plt
import matplotlib
from matplotlib.collections import LineCollection
import math
from moviepy.editor import VideoClip
import random
from functools import reduce
from moviepy.video.io.bindings import mplfig_to_npimage
from operator import mul
import numpy as np
import colorsys
import logging
from importlib import reload
from typing import Generator
from Util.dimensions import Dimensions

matplotlib.use("Agg")
reload(logging)

logger = logging.getLogger()
logger.setLevel(logging.DEBUG)

logging.basicConfig(
    level=logging.DEBUG,
    format="%(asctime)s.%(msecs)03d %(levelname)s: %(message)s",
    datefmt="%Y-%m-%d %H:%M:%S",
)


def logColor(
    distance: float, base: float, const: float, scale: float, modifyingColor: np.array
) -> np.array:
    color = -1 * math.log(distance, base)
    rgb = colorsys.hsv_to_rgb(const + scale * color, 0.8, 0.9)
    return (np.array(rgb) + 2 * modifyingColor) / 3


class Point:
    def __init__(self, x, y, color):
        self.x = x
        self.y = y
        self.color = color


colors = [
    [255, 180, 0],  # yellow-er orange
    [255, 149, 0],  # orange
    [95, 190, 255],  # blue
    [255, 29, 170],  # pink
    [107, 209, 62],  # green
]
colors = np.array(list(map(lambda arr: np.array(arr) / 255, colors)))


class Function:
    def __init__(self, numCoeff):
        self.coeffs = [0] * numCoeff
        self.powers = [0] * numCoeff
        self.modifyingColor = random.choice(colors)
        while reduce(mul, self.coeffs) * reduce(mul, self.powers) == 0:
            self.coeffs = [random.randrange(1, 200) / 100 for i in range(numCoeff)]
            self.powers = [
                random.randrange(3, 30, 2) * random.choice([1, -1])
                for i in range(numCoeff)
            ]

    def eval(self, t: np.array) -> Generator[Point, None, None]:
        x = np.zeros(len(t))
        y = np.zeros(len(t))

        for coeff, power in zip(self.coeffs, self.powers):
            x = x + coeff * np.cos(power * t)
            y = y + coeff * np.sin(power * t)

        for i in range(len(t)):
            _x = x[i]
            _y = y[i]
            _t = t[i]
            color = logColor(
                (_t + 1) / (math.pi * 2 + 1), math.e, 0.27, 1.0, self.modifyingColor
            )
            yield Point(_x, _y, color)

    def __str__(self):
        return repr(self)

    def __repr__(self):
        return " + ".join(
            f"{coeff} * e ^ ({power}t)"
            for coeff, power in zip(self.coeffs, self.powers)
        ).replace("+ -", "- ")


fig = plt.figure(frameon=False, figsize=np.array(Dimensions._720) / 100)
ax = plt.Axes(fig, [0, 0, 1, 1])
fig.add_axes(ax)
ax.set_facecolor("black")
ax.set_xticks([])
ax.set_yticks([])


def buidSlideShow(
    t, xCoords2d, yCoords2d, colors2d, xMax, yMax, numSecondsPerImage, numFunctions
):
    imageNumber = math.floor(t / numSecondsPerImage)
    fractionalBit = t
    while fractionalBit >= numSecondsPerImage:
        fractionalBit -= numSecondsPerImage

    global fig, ax
    ax.cla()
    ax.set_xlim(-xMax * 1.25, xMax * 1.25)
    ax.set_ylim(-yMax * 1.25, yMax * 1.25)

    newXCoords = (
        xCoords2d[imageNumber % numFunctions] * (numSecondsPerImage - fractionalBit)
        + xCoords2d[(imageNumber + 1) % numFunctions] * fractionalBit
    ) / numSecondsPerImage
    newYCoords = (
        yCoords2d[imageNumber % numFunctions] * (numSecondsPerImage - fractionalBit)
        + yCoords2d[(imageNumber + 1) % numFunctions] * fractionalBit
    ) / numSecondsPerImage
    newColors = (
        colors2d[imageNumber % numFunctions] * (numSecondsPerImage - fractionalBit)
        + colors2d[(imageNumber + 1) % numFunctions] * fractionalBit
    ) / numSecondsPerImage

    ax.add_collection(
        LineCollection(
            [
                np.column_stack((newXCoords[i : i + 2], newYCoords[i : i + 2]))
                for i in range(len(newXCoords - 1))
            ],
            linewidths=2,
            colors=newColors,
            ls="dotted",
        )
    )

    return mplfig_to_npimage(fig)


def main():
    numFunctions = 10
    functions = [Function(3) for i in range(numFunctions)]

    numPoints = int(25e2) // 2
    points2d = [[] for _ in range(numFunctions)]

    for i in range(numFunctions):
        t = np.linspace(0, 2 * math.pi, numPoints)
        points2d[i].extend(functions[i].eval(t))

    xCoords2d = []
    yCoords2d = []
    colors2d = []
    for i in range(numFunctions):
        xCoords2d.append(list(map(lambda p: p.x, points2d[i])))
        yCoords2d.append(list(map(lambda p: p.y, points2d[i])))
        colors2d.append(list(map(lambda p: p.color, points2d[i])))
        xCoords2d[i].append(xCoords2d[i][0])
        yCoords2d[i].append(yCoords2d[i][0])

    xCoords2d = np.array(xCoords2d)
    yCoords2d = np.array(yCoords2d)
    colors2d = np.array(colors2d)

    xMax = max(map(max, map(abs, xCoords2d)))
    yMax = max(map(max, map(abs, yCoords2d)))

    # constants
    fps = 30
    numSecondsPerImage = 4
    duration = numFunctions * numSecondsPerImage

    logging.info(f"Total duration = {duration} seconds")

    animation = VideoClip(
        lambda t: buidSlideShow(
            t,
            xCoords2d,
            yCoords2d,
            colors2d,
            xMax,
            yMax,
            numSecondsPerImage,
            numFunctions,
        ),
        duration=duration,
    )
    animation.write_videofile("./Videos/Complex.mp4", fps=fps)


if __name__ == "__main__":
    main()
