import matplotlib.pyplot as plt
import numpy as np
import colorsys
from moviepy.video.io.bindings import mplfig_to_npimage
from moviepy.editor import VideoClip
import matplotlib

matplotlib.use("Agg")


def squares(t):
    fig = plt.figure(frameon=False, figsize=(5, 5))
    ax = plt.Axes(fig, [0, 0, 1, 1])
    fig.add_axes(ax)
    ax.set_facecolor("black")
    ax.set_aspect("equal")
    ax.set_xticks([])
    ax.set_yticks([])
    theta = np.linspace(0, 2 * np.pi, 1000)
    k = abs(np.sin(t)) / 20 + 0.04
    for A in range(1, 1000):
        r = 1 / (
            ((np.sin(k) + np.cos(k)) ** A)
            * (
                np.abs(np.sin(theta - A * k - np.pi / 4))
                + np.abs(np.cos(theta - A * k - np.pi / 4))
            )
        )

        color = colorsys.hsv_to_rgb((np.pi * A / 200) % 1, 1, 1)

        x = r * np.cos(theta)
        y = r * np.sin(theta)
        ax.fill(x, y, color=color)

    return mplfig_to_npimage(fig)


def main():
    duration, fps = 10, 30
    animation = VideoClip(
        squares,
        duration=duration,
    )
    animation.write_videofile("./Videos/Squares.mp4", fps=fps)


if __name__ == "__main__":
    main()
