import matplotlib.pyplot as plt
import matplotlib
import numpy as np
import colorsys
import random
from Util.functions import buildExpr

matplotlib.use("Agg")


def trigInequality(expr1, color1, color2):
    fig = plt.figure(figsize=(10, 10), facecolor=color2)
    ax = plt.Axes(fig, [0, 0, 1, 1])
    ax.set_xticks([])
    ax.set_yticks([])
    ax.set_xlim(-1, 1)
    ax.set_ylim(-1, 1)
    ax.set_aspect("equal")
    ax.axis("off")

    fig.add_axes(ax)

    numDivs = int(2e3)
    xCoords = []
    yCoords = []

    for x in np.linspace(-1, 1, numDivs):
        y = np.linspace(-1, 1, numDivs)

        matches = y[expr1.eval(x, y) < expr1.eval(y, x)]
        xCoords.extend([x] * len(matches))
        yCoords.extend(matches)

    ax.scatter(xCoords, yCoords, color=color1, s=0.5)

    xCoords = []
    yCoords = []

    return fig


def main():
    expr1 = buildExpr()
    hue1 = random.uniform(0, 1)
    hue2 = (hue1 + 0.5) % 1

    print(f"f(x, y) = {expr1}")
    color1 = colorsys.hsv_to_rgb(hue1, 1, 1)
    color2 = colorsys.hsv_to_rgb(hue2, 1, 1)
    print(color1, color2)

    fig = trigInequality(expr1, color1, color2)

    fig.savefig("./Images/TrigInequality.png")


if __name__ == "__main__":
    main()
