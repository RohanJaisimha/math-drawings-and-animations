import cv2
import numpy as np
import matplotlib.pyplot as plt
import matplotlib
from moviepy.editor import VideoFileClip
from moviepy.video.io.bindings import mplfig_to_npimage
import colorsys
import random
from PIL import Image

matplotlib.use("Agg")


def sobel_edge_detector(grayScaleImage, threshold=50):
    # Apply Sobel operator
    sobel_x = cv2.Sobel(grayScaleImage, cv2.CV_64F, 1, 0, ksize=3)
    sobel_y = cv2.Sobel(grayScaleImage, cv2.CV_64F, 0, 1, ksize=3)

    # Calculate gradient magnitude
    magnitude = np.sqrt(sobel_x**2 + sobel_y**2)

    # Apply a threshold to identify edges
    edges = magnitude > threshold

    return edges


def makeWeird(image, modifier):
    assert 0 <= modifier <= 1, f"{modifier}"

    imageGrayscale = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

    # map from [0, 1] to [25, 50]
    sobelThreshold = abs(np.cos(modifier * 10 * np.pi))
    sobelThreshold = sobelThreshold * 50 + 25
    edge_image = sobel_edge_detector(imageGrayscale, sobelThreshold)

    fig = plt.figure(frameon=False)
    fig.patch.set_facecolor("black")
    ax = plt.Axes(fig, [0, 0, 1, 1])
    fig.add_axes(ax)
    ax.set_xticks([])
    ax.set_yticks([])

    # hue
    hue = (10 * modifier) % 1
    ax.set_facecolor(colorsys.hsv_to_rgb(hue, 1.0, 1.0))

    cdict = {
        "red": (
            (0.0, 0.0, 0.0),
            (0.1, 0.5, 0.5),
            (0.2, 0.0, 0.0),
            (0.4, 0.2, 0.2),
            (0.6, 0.0, 0.0),
            (0.8, 1.0, 1.0),
            (1.0, 1.0, 1.0),
        ),
        "green": (
            (0.0, 0.0, 0.0),
            (0.1, 0.0, 0.0),
            (0.2, 0.0, 0.0),
            (0.4, 1.0, 1.0),
            (0.6, 1.0, 1.0),
            (0.8, 1.0, 1.0),
            (1.0, 0.0, 0.0),
        ),
        "blue": (
            (0.0, 0.0, 0.0),
            (0.1, 0.5, 0.5),
            (0.2, 1.0, 1.0),
            (0.4, 1.0, 1.0),
            (0.6, 0.0, 0.0),
            (0.8, 0.0, 0.0),
            (1.0, 0.0, 0.0),
        ),
    }

    my_cmap = matplotlib.colors.LinearSegmentedColormap("my_colormap", cdict, 256)

    # map modifier from [0, 1] onto [0.7, 0.8]
    transparencyModifier = abs(np.sin(modifier * 10 * np.pi))
    filterTransparency = transparencyModifier * 0.1 + 0.7
    ax.imshow(imageGrayscale, cmap=plt.cm.gray, alpha=1 - filterTransparency)
    ax.imshow(edge_image, cmap=my_cmap, alpha=filterTransparency)

    return mplfig_to_npimage(fig)


def main():
    image = makeWeird(
        cv2.imread("Images/Input.png", cv2.COLOR_BGR2RGB), random.uniform(0, 1)
    )
    Image.fromarray(image).save("./Images/Sobel.png")

    clip = VideoFileClip("Videos/Input.mp4")

    plt.ioff()

    numFrames = sum(1 for _ in clip.iter_frames())
    frameNum = -1

    def makeWeirdWrapper(frame):
        global frameNum, numFrames
        frameNum += 1
        return makeWeird(frame, frameNum / numFrames)

    clip.fl_image(makeWeirdWrapper).write_videofile("Videos/Sobel.mp4")


if __name__ == "__main__":
    main()
