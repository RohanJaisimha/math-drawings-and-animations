import numpy as np
import cv2
from moviepy.editor import VideoClip
import random
import colorsys


def circles(t):
    frame = np.zeros((800, 1400, 4), np.uint8)

    # 0 <= color_modifier < 1
    color_modifier = (np.sin(t / np.pi) + 1) / 2

    # 2 <= radius_modifier <= 257
    radius_modifier = round(np.sin(t / 2.25) * 127.5 + 127.5)
    for x in range(0, 1800, 200):
        for y in range(0, 1400, 200):
            if radius_modifier >= 2:
                cv2.circle(
                    frame,
                    (x, y),
                    radius_modifier,
                    np.array(colorsys.hsv_to_rgb(color_modifier, 1, 1)) * 255,
                    3,
                )
            if 255 - radius_modifier >= 2:
                cv2.circle(
                    frame,
                    (x, y),
                    255 - radius_modifier,
                    np.array(colorsys.hsv_to_rgb(color_modifier + 0.5, 1, 1)) * 255,
                    3,
                )

    return cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)


def main():
    slideshow = VideoClip(
        lambda t: circles(t),
        duration=30,
    )

    slideshow.write_videofile("./Videos/Circles.mp4", fps=60)


if __name__ == "__main__":
    main()
