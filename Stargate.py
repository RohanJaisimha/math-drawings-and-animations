import random
from moviepy.editor import VideoClip
import matplotlib.pyplot as plt
import matplotlib
import logging
from importlib import reload
from moviepy.video.io.bindings import mplfig_to_npimage
from typing import List
import colorsys
import math
import numpy as np
from Util.dimensions import Dimensions

matplotlib.use("Agg")

random.seed()

reload(logging)

logger = logging.getLogger()
logger.setLevel(logging.DEBUG)

logging.basicConfig(
    level=logging.DEBUG,
    format="%(asctime)s.%(msecs)03d %(levelname)s %(module)s - %(funcName)s: %(message)s",
    datefmt="%Y-%m-%d %H:%M:%S",
)


w, h = Dimensions._1080


def logColor(distance: float, base: float, const: float, scale: float) -> np.array:
    color = -1 * math.log(distance, base)
    rgb = colorsys.hsv_to_rgb(const + scale * color, 0.8, 0.9)
    return np.array(rgb)


class PlottingDeet:
    def __init__(self, xCoord: float, yCoord: float, size: float, color: List[float]):
        self.xCoord = xCoord
        self.yCoord = yCoord
        self.size = size
        self.color = color


class Star:
    def __init__(self):
        self.x = random.uniform(-w / 2, w / 2)
        self.y = random.uniform(-h / 2, h / 2)
        self.z = random.uniform(0, w)

    def update(self):
        self.z -= 25
        if self.z < 1:
            self.x = random.uniform(-w / 2, w / 2)
            self.y = random.uniform(-h / 2, h / 2)
            self.z = random.uniform(0, w)

    def getPlottingDeetz(self, t, duration):
        return PlottingDeet(
            self.x / self.z * w,
            self.y / self.z * h,
            20 - 20 * self.z / w,
            self.getColor(t, duration),
        )

    def getColor(self, t, duration) -> np.array:
        if self.x < 0:
            color = logColor((t + 1) / (duration + 1), math.e, 0.27, 1.0)
            pink = np.array([255, 34, 129]) / 255
            return (color + 4 * pink) / 5
        else:
            color = logColor((t + 1) / (duration + 1), math.e, 0.27, 1.0)
            purple = np.array([183, 108, 253]) / 255
            return (color + 4 * purple) / 5


def makeClip(t, stars: List[Star], duration):
    global fig, ax

    ax.cla()
    fig = plt.figure(frameon=False, figsize=(w / 100, h / 100))
    ax = plt.Axes(fig, [0, 0, 1, 1])
    fig.add_axes(ax)
    ax.set_facecolor("black")
    ax.set_xticks([])
    ax.set_yticks([])
    ax.set_xlim(-w / 2, w / 2)
    ax.set_ylim(-h / 2, h / 2)

    plottingDeetz = []
    for i in range(len(stars)):
        stars[i].update()
        plottingDeetz.append(stars[i].getPlottingDeetz(t, duration))

    xCoords = list(map(lambda p: p.xCoord, plottingDeetz))
    yCoords = list(map(lambda p: p.yCoord, plottingDeetz))
    markerSizes = list(map(lambda p: p.size, plottingDeetz))
    colors = list(map(lambda p: p.color, plottingDeetz))

    ax.scatter(xCoords, yCoords, s=markerSizes, c=colors)

    return mplfig_to_npimage(fig)


def main():
    duration = 30
    fps = 60
    stars = [Star() for i in range(1500)]
    # build the slideshow
    slideshow = VideoClip(lambda t: makeClip(t, stars, duration), duration=duration)

    slideshow.write_videofile("./Videos/Stargate.mp4", fps=fps)


if __name__ == "__main__":
    main()
