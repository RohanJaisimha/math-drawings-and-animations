import matplotlib.pyplot as plt
import numpy as np
from moviepy.video.io.bindings import mplfig_to_npimage
from moviepy.editor import VideoClip
import matplotlib
import colorsys

matplotlib.use("Agg")


def squares(t):
    fig = plt.figure(frameon=False, figsize=(5, 5))
    ax = plt.Axes(fig, [0, 0, 1, 1])
    fig.add_axes(ax)
    ax.set_facecolor("black")
    ax.set_aspect("equal")
    ax.set_xticks([])
    ax.set_yticks([])
    x = [1]
    y = [1]

    t_slider = np.cos(t) * 600 + 9400

    for i in range(50):
        last_x = x[-1]
        last_y = y[-1]

        if i % 4 in [0, 1]:
            next_x = (
                -np.sqrt(t_slider) * np.abs(last_x + last_y) + 100 * (last_x - last_y)
            ) / 200
            next_y = (
                100 * last_x**2
                + 200 * last_x * last_y
                + np.sqrt(t_slider) * np.abs(last_x + last_y) * (last_x - last_y)
                + 100 * last_y**2
            ) / (200 * (last_x + last_y))
        else:
            next_x = (
                np.sqrt(t_slider) * np.abs(last_x + last_y) + 100 * (last_x - last_y)
            ) / 200
            next_y = (
                100 * last_x**2
                + 200 * last_x * last_y
                - np.sqrt(t_slider) * np.abs(last_x + last_y) * (last_x - last_y)
                + 100 * last_y**2
            ) / (200 * (last_x + last_y))

        x.append(next_x)
        y.append(next_y)

    for i, [_x, _y] in enumerate(zip(x, y)):
        ax.fill(
            [_x, _y, -_x, -_y, _x],
            [_y, -_x, -_y, _x, _y],
            c="white" if i % 2 else "black",
        )

    return mplfig_to_npimage(fig)


def main():
    duration, fps = np.ceil(10 / np.pi) * np.pi, 30
    animation = VideoClip(
        squares,
        duration=duration,
    )
    animation.write_videofile("./Videos/SquaresAccordion.mp4", fps=fps)
    squares(10)


if __name__ == "__main__":
    main()
