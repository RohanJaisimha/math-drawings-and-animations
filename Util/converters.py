import numpy as np
from PIL import Image


def convertImageToArray(image: Image) -> np.array:
    return np.array(image)


def convertArrayToImage(arr: np.array) -> Image:
    return Image.fromarray(arr)
