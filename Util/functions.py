import random
from abc import ABC, abstractmethod
import numpy as np


class Function(ABC):
    @abstractmethod
    def eval(self, x: float, y: float) -> float:
        pass

    @abstractmethod
    def __str__():
        pass


class X(Function):
    def eval(self, x: float, y: float) -> float:
        return x

    def __str__(self):
        return "x"


class Y(Function):
    def eval(self, x: float, y: float) -> float:
        return y

    def __str__(self):
        return "y"


class SinPi(Function):
    def __init__(self, prob: float):
        self.arg = buildExpr(prob ** 2)

    def __str__(self):
        return f"sin(pi * {self.arg})"

    def eval(self, x: float, y: float) -> float:
        return np.sin(np.pi * self.arg.eval(x, y))


class CosPi(Function):
    def __init__(self, prob):
        self.arg = buildExpr(prob ** 2)

    def __str__(self):
        return f"cos(pi * {self.arg})"

    def eval(self, x: float, y: float) -> float:
        return np.cos(np.pi * self.arg.eval(x, y))


class Times(Function):
    def __init__(self, prob):
        self.lhs = buildExpr(prob ** 2)
        self.rhs = buildExpr(prob ** 2)

    def __str__(self):
        return f"{self.lhs} * {self.rhs}"

    def eval(self, x: float, y: float) -> float:
        return self.lhs.eval(x, y) * self.rhs.eval(x, y)


class Plus(Function):
    def __init__(self, prob):
        self.lhs = buildExpr(prob ** 2)
        self.rhs = buildExpr(prob ** 2)

    def __str__(self):
        return f"({self.lhs} + {self.rhs})"

    def eval(self, x: float, y: float) -> float:
        return self.lhs.eval(x, y) + self.rhs.eval(x, y)


class TimesConstant(Function):
    def __init__(self, prob):
        self.k = random.randrange(1, 6) * (-1 if random.uniform(0, 1) > 0.5 else 1)
        self.arg = buildExpr(prob ** 2)

    def __str__(self):
        return f"{self.k} * {self.arg}"

    def eval(self, x: float, y: float) -> float:
        return self.k * self.arg.eval(x, y)


class PlusConstant(Function):
    def __init__(self, prob):
        self.k = random.randrange(1, 6) * (-1 if random.uniform(0, 1) > 0.5 else 1)
        self.arg = buildExpr(prob ** 2)

    def __str__(self):
        return f"({self.k} + {self.arg})"

    def eval(self, x: float, y: float) -> float:
        return self.k + self.arg.eval(x, y)


class Power(Function):
    def __init__(self, prob):
        self.k = random.randrange(2, 6)
        self.arg = buildExpr(prob ** 2)

    def __str__(self):
        return f"({self.arg} ** {self.k})"

    def eval(self, x: float, y: float) -> float:
        return self.arg.eval(x, y) ** self.k


class Exp(Function):
    def __init__(self, prob):
        self.arg = buildExpr(prob ** 2)

    def __str__(self):
        return f"(e ** {self.arg})"

    def eval(self, x: float, y: float) -> float:
        return np.exp(self.arg.eval(x, y))


def buildExpr(prob: float = 0.99) -> Function:
    if random.uniform(0, 1) < prob:
        return random.choice([SinPi, CosPi, Times, Power, TimesConstant, Plus, Exp])(
            prob
        )
    else:
        return random.choice([X, Y])()
