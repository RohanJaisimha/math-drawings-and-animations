from PIL import Image, ImageOps, ImageEnhance
import random
from typing import Tuple
import logging

Color = Tuple[int, int, int]
logger = logging.getLogger()
logger.setLevel(logging.DEBUG)

logging.basicConfig(
    level=logging.DEBUG,
    format="%(asctime)s.%(msecs)03d %(levelname)s: %(message)s",
    datefmt="%Y-%m-%d %H:%M:%S",
)


def deepfry(image: Image, log: bool = True) -> Image:
    return sharpen(
        imageOverlay(posterize(resize(image, log=log), log=log), log=log), log=log
    )


def resize(image: Image, log: bool = True) -> Image:
    # Crush image to hell and back
    if log:
        logging.info("resizing")
    width, height = image.width, image.height
    image = image.resize(
        (int(width ** 0.75), int(height ** 0.75)), resample=Image.LANCZOS
    )
    image = image.resize(
        (int(width ** 0.88), int(height ** 0.88)), resample=Image.BILINEAR
    )
    image = image.resize(
        (int(width ** 0.9), int(height ** 0.9)), resample=Image.BICUBIC
    )
    image = image.resize((width, height), resample=Image.BICUBIC)
    return image


def posterize(
    image: Image, posterizeCoefficient: int = None, log: bool = True
) -> Image:
    if posterizeCoefficient is None:
        posterizeCoefficient = random.randrange(2, 5)
    if log:
        logging.info(f"posterizing with posterizeCoefficient={posterizeCoefficient}")
    return ImageOps.posterize(image, posterizeCoefficient)


def solarize(image: Image, solarizeCoefficient: int = None, log: bool = True) -> Image:
    if solarizeCoefficient is None:
        solarizeCoefficient = random.randrange(0, 256)
    if log:
        logging.info(f"solarizing with solarizeCoefficient={solarizeCoefficient}")
    return ImageOps.solarize(image, solarizeCoefficient)


def imageOverlay(
    image,
    color1: Color = None,
    color2: Color = None,
    blendingCoefficient: float = None,
    contrastCoefficient: float = None,
    log: bool = True,
) -> Image:
    if color1 is None:
        color1 = (
            random.randrange(0, 256),
            random.randrange(0, 256),
            random.randrange(0, 256),
        )
    if color2 is None:
        color2 = (
            random.randrange(0, 256),
            random.randrange(0, 256),
            random.randrange(0, 256),
        )
    if blendingCoefficient is None:
        blendingCoefficient = random.uniform(0.25, 0.75)
    if contrastCoefficient is None:
        contrastCoefficient = random.uniform(0, 10)
    if log:
        logging.info(
            f"overlaying with color1={color1}, color2={color2}, blendingCoefficient={blendingCoefficient}, and contrastCoefficient={contrastCoefficient}"
        )

    # Generate color overlay
    overlay = image.split()[0]
    overlay = ImageEnhance.Contrast(overlay).enhance(contrastCoefficient)
    overlay = ImageEnhance.Brightness(overlay).enhance(1.5)
    overlay = ImageOps.colorize(overlay, color1, color2)

    # Overlay color overlay and main image
    return Image.blend(image, overlay, blendingCoefficient)


def sharpen(
    image: Image, sharpeningCoefficient: float = None, log: bool = True
) -> Image:
    if sharpeningCoefficient is None:
        sharpeningCoefficient = random.randrange(2, 100)
    if log:
        logging.info(f"sharpening with sharpeningCoefficient={sharpeningCoefficient}")
    return ImageEnhance.Sharpness(image).enhance(sharpeningCoefficient)


def randomFilters(image: Image, log: bool = True) -> Image:
    filterFunctions = [sharpen, posterize, resize, solarize]

    for filterFunction in random.sample(
        filterFunctions,
        random.randrange(len(filterFunctions) // 2, len(filterFunctions) + 1),
    ):
        image = filterFunction(image, log=log)

    return image
