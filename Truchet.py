import matplotlib.pyplot as plt
import matplotlib
import matplotlib.patches as patches
import random
import numpy as np

matplotlib.use("Agg")


def truchetFilledCircles(grid):
    m = len(grid)
    n = len(grid[0])
    fig = plt.figure(figsize=(n / 2.5, m / 2.5))
    ax = plt.Axes(fig, [0, 0, 1, 1])
    fig.add_axes(ax)
    ax.set_facecolor("white")
    ax.set_xticks([])
    ax.set_yticks([])
    ax.set_xlim(0, n)
    ax.set_ylim(0, m)

    ax.set_aspect("equal")
    ax.axis("off")

    ax.plot([0, n, n, 0, 0], [0, 0, m, m, 0], "black")

    radius = 0.5

    for i in range(m):
        for j in range(n):
            if grid[i][j] == 1:
                pass
                ax.add_patch(
                    patches.Wedge((j, i + 1), radius, 270, 360, facecolor="black")
                )  # TL
                ax.add_patch(
                    patches.Wedge((j + 1, i), radius, 90, 180, facecolor="black")
                )  # BR
            else:
                ax.add_patch(
                    patches.Wedge((j, i), radius, 0, 90, facecolor="black")
                )  # BL
                ax.add_patch(
                    patches.Wedge((j + 1, i + 1), radius, 180, 270, facecolor="black")
                )  # TR

    return fig


m = 18
n = 32


def truchetEmptyCircles(grid):
    m = len(grid)
    n = len(grid[0])
    fig = plt.figure(figsize=(n / 2.5, m / 2.5))
    ax = plt.Axes(fig, [0, 0, 1, 1])
    fig.add_axes(ax)
    ax.set_facecolor("white")
    ax.set_xticks(np.arange(0, n, 0.5))
    ax.set_yticks(np.arange(0, m, 0.5))
    ax.set_xlim(0, n)
    ax.set_ylim(0, m)

    ax.set_aspect("equal")
    ax.axis("off")

    ax.plot([0, n, n, 0, 0], [0, 0, m, m, 0], "black")

    radius = 1

    for i in range(m):
        for j in range(n):
            if grid[i][j] == 1:
                pass
                ax.add_patch(
                    patches.Arc(
                        (j, i + 1), radius, radius, 0, 270, 360, facecolor="black"
                    )
                )  # TL
                ax.add_patch(
                    patches.Arc(
                        (j + 1, i), radius, radius, 0, 90, 180, facecolor="black"
                    )
                )  # BR
            else:
                ax.add_patch(
                    patches.Arc((j, i), radius, radius, 0, 0, 90, facecolor="black")
                )  # BL
                ax.add_patch(
                    patches.Arc(
                        (j + 1, i + 1), radius, radius, 0, 180, 270, facecolor="black"
                    )
                )  # TR

    return fig


def main():
    m = 18
    n = 32
    grid = [[random.randrange(0, 2) for j in range(n)] for i in range(m)]
    fig = truchetFilledCircles(grid)
    fig.savefig("./Images/TruchetFilled.png")

    grid = [[random.randrange(0, 2) for j in range(n)] for i in range(m)]
    fig = truchetEmptyCircles(grid)
    fig.savefig("./Images/TruchetEmpty.png")


if __name__ == "__main__":
    main()
