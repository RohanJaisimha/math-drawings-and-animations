import numpy as np
from moviepy.editor import VideoClip
import matplotlib
from typing import List
import logging
from importlib import reload
import colorsys
import math
from Util.dimensions import Dimensions
from numba import jit

matplotlib.use("Agg")
reload(logging)

logger = logging.getLogger()
logger.setLevel(logging.DEBUG)

logging.basicConfig(
    level=logging.DEBUG,
    format="%(asctime)s.%(msecs)03d %(levelname)s: %(message)s",
    datefmt="%Y-%m-%d %H:%M:%S",
)


def logColor(distance: float, base: float, const: float, scale: float) -> float:
    color = -1 * math.log(distance, base)
    rgb = colorsys.hsv_to_rgb(const + scale * color, 0.8, 0.9)
    return tuple(round(i * 255) for i in rgb)


INF = 4
CENTER_X = -0.7106
CENTER_Y = 0.246575
MULTIPLIER = 4
FPS = 20
DURATION = 10


@jit
def mandelbrotHelper(C: np.array, maxIterations: float) -> np.array:
    Z = np.zeros(C.shape, dtype=complex)
    M = np.full(C.shape, True, dtype=bool)
    count = np.full(C.shape, maxIterations, dtype=int)
    for i in range(math.ceil(maxIterations)):
        Z[M] = Z[M] * Z[M] + C[M]
        M[np.abs(Z) > INF] = False

        count[np.logical_not(M)] = np.minimum(count[np.logical_not(M)], i)

    return count


@jit
def mandelbrot(
    minX: float, maxX: float, minY: float, maxY: float, canvasDimensions: List[int]
) -> np.array:
    x = np.linspace(minX, maxX, canvasDimensions[0]).reshape((1, canvasDimensions[0]))
    y = np.linspace(minY, maxY, canvasDimensions[1]).reshape((canvasDimensions[1], 1))
    maxIterations = 1e3  # min(1e2, 50 + math.log(4 / (maxX - minX), 10) ** 5)
    C = np.tile(x, (canvasDimensions[1], 1)) + 1j * np.tile(y, (1, canvasDimensions[0]))

    count = mandelbrotHelper(C, maxIterations)
    toReturn = (count + 1) / (maxIterations + 1)
    toReturn = np.repeat(toReturn[:, :, np.newaxis], 3, axis=2)
    toReturn[:, :, 1:] = [1]

    return matplotlib.colors.hsv_to_rgb(toReturn) * 255


def main():
    w, h = Dimensions._720

    slideshow = VideoClip(
        lambda t: mandelbrot(
            minX=CENTER_X - w / (500 * MULTIPLIER**t),
            maxX=CENTER_X + w / (500 * MULTIPLIER**t),
            minY=CENTER_Y - h / (500 * MULTIPLIER**t),
            maxY=CENTER_Y + h / (500 * MULTIPLIER**t),
            canvasDimensions=[w, h],
        ),
        duration=DURATION,
    )

    slideshow.write_videofile("./Videos/mandelbrot.mp4", fps=FPS, threads=2)


if __name__ == "__main__":
    main()
