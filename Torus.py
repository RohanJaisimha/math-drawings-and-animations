import matplotlib.pyplot as plt
import matplotlib
import numpy as np
import colorsys
from moviepy.editor import VideoClip
from moviepy.video.io.bindings import mplfig_to_npimage

matplotlib.use("Agg")


def torus(t, color1, color2):
    limit = 0.25
    fig = plt.figure(figsize=(10, 10), facecolor=color2)
    ax = plt.Axes(fig, [0, 0, 1, 1])
    fig.add_axes(ax)
    ax.set_xticks([])
    ax.set_yticks([])
    ax.set_xlim(-limit, limit)
    ax.set_ylim(-limit, limit)

    ax.set_aspect("equal")
    ax.axis("off")

    # equation: (x - c) ** 2 + (y - d) ** 2 <= k * exp(2 sin (kx / (x ^ 2 + y ^2)) - p) + exp(2 cos (ky / (x ^ 2 + y ^2)) - p)
    xCoords = []
    yCoords = []

    # keep k on [-3, -1]
    k = -1  # np.sin(t * np.pi / 5) - 2

    numDivs = int(1e3)
    for x in np.linspace(-limit, limit, numDivs):
        y = np.linspace(-limit, limit, numDivs)
        matches = y[
            (x - 1.5) ** 2 + (y + 1) ** 2
            < k * np.exp(2 * np.sin(k * x / (x**2 + y**2) - 2 * t))
            + np.exp(2 * np.cos(k * y / (x**2 + y**2) - 2 * t))
        ]
        xCoords.extend([x] * len(matches))
        yCoords.extend(matches)

    ax.scatter(xCoords, yCoords, color=color1, s=2)

    return fig


def torusHelper(t):
    hue1 = np.cos(0.5 * t) / 2 + 0.5
    hue2 = (hue1 + 0.5) % 1
    color1 = colorsys.hsv_to_rgb(hue1, 1, 1)
    color2 = (0, 0, 0)  # colorsys.hsv_to_rgb(hue2, 1, 1)

    return mplfig_to_npimage(torus(t, color1, color2))


def main():
    animation = VideoClip(torusHelper, duration=20)

    animation.write_videofile("./Videos/Torus.mp4", fps=30, threads=2)


if __name__ == "__main__":
    main()
