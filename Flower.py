import matplotlib.pyplot as plt
import matplotlib
import numpy as np
import colorsys
from moviepy.editor import VideoClip
from moviepy.video.io.bindings import mplfig_to_npimage
import random

matplotlib.use("Agg")

THETA = np.linspace(0, 2 * np.pi, int(1e4))


def flowerPulsing(t: float) -> np.array:
    # -30 <= n <= 30
    n = 30
    # -1<= a <= 1
    a = np.sin(t) + 0.3 * np.sin(10 * t)

    fig = plt.figure(figsize=(10, 10), facecolor=[0, 0, 0])
    ax = plt.Axes(fig, [0, 0, 1, 1])
    ax.set_xticks([])
    ax.set_yticks([])
    ax.set_xlim(-15, 15)
    ax.set_ylim(-15, 15)
    ax.set_aspect("equal")
    ax.axis("off")

    fig.add_axes(ax)

    for r in np.arange(12, -1, -1):
        ax.fill(
            (r + a * np.sin(n * THETA)) * np.cos(THETA),
            (r + a * np.sin(n * THETA)) * np.sin(THETA),
            color=(
                colorsys.hsv_to_rgb((np.cos(t * r / 5) + 1) / 2, 1, 1)
                if r % 2
                else (0, 0, 0)
            ),
        )

    return mplfig_to_npimage(fig)


def flowerRevolving(t: float) -> np.array:
    # -30 <= n <= 30
    n = 30
    # -1<= a <= 1
    a = -1

    fig = plt.figure(figsize=(10, 10), facecolor=[0, 0, 0])
    ax = plt.Axes(fig, [0, 0, 1, 1])
    ax.set_xticks([])
    ax.set_yticks([])
    ax.set_xlim(-20, 20)
    ax.set_ylim(-20, 20)
    ax.set_aspect("equal")
    ax.axis("off")

    fig.add_axes(ax)

    for r in [9, 15]:
        ax.plot(
            (r + a * np.sin(n * THETA - 2 * np.pi * t)) * np.cos(THETA),
            (r + a * np.sin(n * THETA - 2 * np.pi * t)) * np.sin(THETA),
            color=colorsys.hsv_to_rgb(0.375, 1, 1),
            lw=3.5,
        )
        ax.plot(
            (r + a * np.cos(n * THETA)) * np.cos(THETA),
            (r + a * np.cos(n * THETA - 2 * np.pi * t)) * np.sin(THETA),
            color=colorsys.hsv_to_rgb(0.875, 1, 1),
            lw=3.5,
        )
    for r in [12, 18]:
        ax.plot(
            (r + a * np.sin(n * THETA + 2 * np.pi * t)) * np.cos(THETA),
            (r + a * np.sin(n * THETA + 2 * np.pi * t)) * np.sin(THETA),
            color=colorsys.hsv_to_rgb(0.375, 1, 1),
            lw=3.5,
        )
        ax.plot(
            (r + a * np.cos(n * THETA + 2 * np.pi * t)) * np.cos(THETA),
            (r + a * np.cos(n * THETA)) * np.sin(THETA),
            color=colorsys.hsv_to_rgb(0.875, 1, 1),
            lw=3.5,
        )

    return mplfig_to_npimage(fig)


def main():
    animation = VideoClip(
        flowerPulsing,
        duration=30,
    )
    animation.write_videofile("./Videos/FlowerPulsing.mp4", fps=30)

    animation = VideoClip(
        flowerRevolving,
        duration=30,
    )
    animation.write_videofile("./Videos/FlowerRevolving.mp4", fps=30)


if __name__ == "__main__":
    main()
