import matplotlib.pyplot as plt
import matplotlib
import matplotlib.patches as patches
import random
import colorsys

matplotlib.use("Agg")


lineWidth = 2.5


def hitomezashi(horizontalLines, verticalLines, primaryColor, secondaryColor):
    m = len(horizontalLines)
    n = len(verticalLines)

    fig = plt.figure(frameon=False, figsize=(n / 5, m / 5))
    ax = plt.Axes(fig, [0, 0, 1, 1])
    fig.add_axes(ax)
    ax.set_facecolor("black")
    ax.set_xticks([])
    ax.set_yticks([])
    ax.set_xlim(0, n + 1)
    ax.set_ylim(0, m + 1)

    ax.set_aspect("equal")
    ax.axis("off")

    ax.plot(
        [0, n + 1, n + 1, 0, 0], [0, 0, m + 1, m + 1, 0], "black", linewidth=lineWidth
    )

    for i in range(m):
        if horizontalLines[i] == 0:
            for j in range(0, n + 1, 2):
                ax.plot([j, j + 1], [i + 1, i + 1], "black", linewidth=lineWidth)
        elif horizontalLines[i] == 1:
            for j in range(1, n + 1, 2):
                ax.plot([j, j + 1], [i + 1, i + 1], "black", linewidth=lineWidth)

    for i in range(n):
        if verticalLines[i] == 0:
            for j in range(0, m + 1, 2):
                ax.plot([i + 1, i + 1], [j, j + 1], "black", linewidth=lineWidth)
        elif verticalLines[i] == 1:
            for j in range(1, m + 1, 2):
                ax.plot([i + 1, i + 1], [j, j + 1], "black", linewidth=lineWidth)

    color = primaryColor

    # first row
    colorGrid = [[None for _ in range(n + 1)] for _ in range(m + 1)]
    for j in range(n + 1):
        thereIsAWallToTheRight = j < n and verticalLines[j] == 0

        colorGrid[0][j] = color
        if thereIsAWallToTheRight:
            color = primaryColor if color == secondaryColor else secondaryColor

    for i in range(1, m + 1):
        for j in range(n + 1):
            # is there a wall above this cell?
            if (j % 2 == 0 and horizontalLines[i - 1] == 0) or (
                j % 2 == 1 and horizontalLines[i - 1] == 1
            ):
                # flip colors from the cell above
                colorGrid[i][j] = (
                    primaryColor
                    if colorGrid[i - 1][j] == secondaryColor
                    else secondaryColor
                )
            else:
                # copy the color of the cell above
                colorGrid[i][j] = colorGrid[i - 1][j]

    for i in range(m + 1):
        for j in range(n + 1):
            ax.add_patch(patches.Rectangle((j, i), 1, 1, facecolor=colorGrid[i][j]))

    return fig


def main():
    m = 18
    n = 32
    horizontalLines = [random.randrange(0, 2) for _ in range(m)]
    verticalLines = [random.randrange(0, 2) for _ in range(n)]

    hue = random.uniform(0, 1)
    saturation = random.uniform(0, 1)
    primaryColor = colorsys.hsv_to_rgb(hue, saturation, 1)
    secondaryColor = colorsys.hsv_to_rgb((hue + 0.5) % 1, saturation, 1)

    print(f"primary={primaryColor}")
    print(f"secondary={secondaryColor}")

    fig = hitomezashi(horizontalLines, verticalLines, primaryColor, secondaryColor)
    fig.savefig("./Images/Hitomezashi.png")


if __name__ == "__main__":
    main()
