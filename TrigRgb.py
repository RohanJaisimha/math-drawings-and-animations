import random
from PIL import Image
import numpy as np
from typing import List
import logging
from importlib import reload
from Util.functions import buildExpr, Function
from Util.filters import randomFilters
from Util.dimensions import Dimensions

random.seed()

reload(logging)

logger = logging.getLogger()
logger.setLevel(logging.DEBUG)

logging.basicConfig(
    level=logging.DEBUG,
    format="%(asctime)s.%(msecs)03d %(levelname)s: %(message)s",
    datefmt="%Y-%m-%d %H:%M:%S",
)


# adapted from deeppyer
class DefaultColors:
    RED = np.asarray([255, 0, 0])
    BLUE = np.asarray([0, 0, 255])
    GREEN = np.asarray([0, 255, 0])
    WHITE = np.asarray([255, 255, 255])


def plotIntensity(
    function: Function, canvasDimensions: List[int] = Dimensions._360
) -> Image:
    canvas = Image.new("L", canvasDimensions)

    # image dimensions work opposite
    for py in range(canvasDimensions[1]):
        for px in range(canvasDimensions[0]):
            # Convert pixel location from [0, canvasWidth) to [-1, 1] coordinates
            x = float(float(px) / float(canvasDimensions[0])) * 2 - 1

            y = float(float(py) / float(canvasDimensions[1])) * 2 - 1

            # evaluate
            z = function.eval(x, y)

            # Scale [-1, 1] result to [0, 255].
            intensity = int((z + 1) * 127.5)
            canvas.putpixel((px, py), intensity)

    return canvas


def makeRandomTrigImage(
    log: bool = True, canvasDimensions: List[int] = Dimensions._360
) -> Image:
    redExp = buildExpr()
    greenExp = buildExpr()
    blueExp = buildExpr()

    if log:
        logging.info("red = " + str(redExp))
        logging.info("green = " + str(greenExp))
        logging.info("blue = " + str(blueExp))

    redPlane = plotIntensity(redExp, canvasDimensions)
    greenPlane = plotIntensity(greenExp, canvasDimensions)
    bluePlane = plotIntensity(blueExp, canvasDimensions)

    return Image.merge("RGB", [redPlane, greenPlane, bluePlane])


def main():
    randomFilters(Image.open("./Images/Input.png")).save("./Images/RandomFilters.png")

    image = makeRandomTrigImage(canvasDimensions=Dimensions._720)
    image.save("./Images/TrigRgb.png")

    randomFilters(image).save("./Images/TrigRgbRandomFilters.png")


if __name__ == "__main__":
    main()
